import os
import sys

os.system('pip install --upgrade git+https://github.com/intake/filesystem_spec')
os.system('pip install -U git+git://github.com/huggingface/transformers')
os.system('pip install --upgrade datasets')
os.system('pip install wandb')
os.system('pip install pandas')
os.system('git clone https://github.com/huggingface/transformers')
os.system('pip install nltk')
os.system('pip install rouge-score')
# os.system('wget https://raw.githubusercontent.com/huggingface/transformers/master/examples/seq2seq/seq2seq_trainer.py')
os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

run_clm_dir= './transformers/examples/pytorch/question-answering/'


cmd = '''python {0}run_qa.py \
  --model_name_or_path bert-base-uncased \
  --dataset_name squad \
  --do_train \
  --do_eval \
  --per_device_train_batch_size 12 \
  --learning_rate 3e-5 \
  --num_train_epochs 2 \
  --max_seq_length 384 \
  --doc_stride 128 \
  --output_dir /mnt/mldisk/squadfi/models
'''.format(run_clm_dir)


print(cmd)

os.system(cmd)