import os
import sys

os.system('pip install --upgrade git+https://github.com/intake/filesystem_spec')
os.system('pip install -U git+git://github.com/huggingface/transformers')
os.system('pip install --upgrade datasets')
os.system('pip install wandb')
os.system('pip install pandas')
os.system('git clone https://github.com/huggingface/transformers')
os.system('pip install nltk')
os.system('pip install rouge-score')
# os.system('wget https://raw.githubusercontent.com/huggingface/transformers/master/examples/seq2seq/seq2seq_trainer.py')
os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

run_clm_dir= './transformers/examples/pytorch/summarization/'

cmd = '''python {0}run_summarization.py \
    --model_name_or_path t5-small \
    --do_train --cache_dir /mnt/mldisk/cache \
    --do_eval \
    --train_file data/askans.csv \
    --validation_file  data/askanseval.csv \
    --source_prefix "summarize: " --save_total_limit 1 --save_steps 2000 \
    --per_device_train_batch_size=32 \
    --per_device_eval_batch_size=32 \
    --output_dir /mnt/mldisk/bert2bert/models  \
    --predict_with_generate
'''.format(run_clm_dir)

print(cmd)

os.system(cmd)

# os.system('pip install rouge_score')

# import datasets
# import transformers
# import pandas as pd
# from datasets import Dataset

# #Tokenizer
# from transformers import RobertaTokenizerFast

# #Encoder-Decoder Model
# from transformers import EncoderDecoderModel

# #Training
# from seq2seq_trainer import Seq2SeqTrainer
# from transformers import TrainingArguments
# from dataclasses import dataclass, field
# from typing import Optional

# import pandas as pd
# df=pd.read_csv("data/Reviews.csv")
# df.drop(columns=['Id', 'ProductId', 'UserId', 'ProfileName', 'HelpfulnessNumerator','HelpfulnessDenominator', 'Score', 'Time'],axis=1,inplace=True)
# df = df.dropna()
# print("Data size:",len(df))
# df.head()

# from datasets import Dataset
# train_data=Dataset.from_pandas(df[:550000])
# val_data=Dataset.from_pandas(df[550000:555000])
# test_data=Dataset.from_pandas(df[556000:557000])

# tokenizer = RobertaTokenizerFast.from_pretrained("roberta-base")
# tokenizer.bos_token = tokenizer.cls_token
# tokenizer.eos_token = tokenizer.sep_token
# #parameter setting
# batch_size=256  #
# encoder_max_length=40
# decoder_max_length=8

# def process_data_to_model_inputs(batch):
#   # tokenize the inputs and labels
#   inputs = tokenizer(batch["Text"], padding="max_length", truncation=True, max_length=encoder_max_length)
#   outputs = tokenizer(batch["Summary"], padding="max_length", truncation=True, max_length=decoder_max_length)

#   batch["input_ids"] = inputs.input_ids
#   batch["attention_mask"] = inputs.attention_mask
#   batch["decoder_input_ids"] = outputs.input_ids
#   batch["decoder_attention_mask"] = outputs.attention_mask
#   batch["labels"] = outputs.input_ids.copy()

#   # because RoBERTa automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`. 
#   # We have to make sure that the PAD token is ignored
#   batch["labels"] = [[-100 if token == tokenizer.pad_token_id else token for token in labels] for labels in batch["labels"]]

#   return batch

# #processing training data
# train_data = train_data.map(
#     process_data_to_model_inputs, 
#     batched=True, 
#     batch_size=batch_size, 
#     remove_columns=["Text", "Summary"]
# )
# train_data.set_format(
#     type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
# )

# #processing validation data
# val_data = val_data.map(
#     process_data_to_model_inputs, 
#     batched=True, 
#     batch_size=batch_size, 
#     remove_columns=["Text", "Summary"]
# )
# val_data.set_format(
#     type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
# )

# from transformers import EncoderDecoderModel

# roberta_shared = EncoderDecoderModel.from_encoder_decoder_pretrained("roberta-base", "roberta-base", tie_encoder_decoder=True)

# # set special tokens
# roberta_shared.config.decoder_start_token_id = tokenizer.bos_token_id                                             
# roberta_shared.config.eos_token_id = tokenizer.eos_token_id

# # sensible parameters for beam search
# # set decoding params                               
# roberta_shared.config.max_length = 40
# roberta_shared.config.early_stopping = True
# roberta_shared.config.no_repeat_ngram_size = 3
# roberta_shared.config.length_penalty = 2.0
# roberta_shared.config.num_beams = 4
# roberta_shared.config.vocab_size = roberta_shared.config.encoder.vocab_size

# # load rouge for validation
# rouge = datasets.load_metric("rouge")

# def compute_metrics(pred):
#     labels_ids = pred.label_ids
#     pred_ids = pred.predictions

#     # all unnecessary tokens are removed
#     pred_str = tokenizer.batch_decode(pred_ids, skip_special_tokens=True)
#     labels_ids[labels_ids == -100] = tokenizer.pad_token_id
#     label_str = tokenizer.batch_decode(labels_ids, skip_special_tokens=True)

#     rouge_output = rouge.compute(predictions=pred_str, references=label_str, rouge_types=["rouge2"])["rouge2"].mid

#     return {
#         "rouge2_precision": round(rouge_output.precision, 4),
#         "rouge2_recall": round(rouge_output.recall, 4),
#         "rouge2_fmeasure": round(rouge_output.fmeasure, 4),
#     }


# training_args = Seq2SeqTrainingArguments(
#     output_dir="./",
#     per_device_train_batch_size=batch_size,
#     per_device_eval_batch_size=batch_size,
#     predict_with_generate=True,
#     evaluate_during_training=True,
#     do_train=True,
#     do_eval=True,
#     logging_steps=2, 
#     save_steps=16, 
#     eval_steps=500, 
#     warmup_steps=500, 
#     overwrite_output_dir=True,
#     save_total_limit=1,
#     fp16=True, 
# )

# # instantiate trainer
# trainer = Seq2SeqTrainer(
#     model=roberta_shared,
#     args=training_args,
#     compute_metrics=compute_metrics,
#     train_dataset=train_data,
#     eval_dataset=val_data,
# )
# trainer.train()