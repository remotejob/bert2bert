#conda create -n gpt2model python=3.7
#pip install torch
#pip install -U git+git://github.com/huggingface/transformers
#unset LIBTORCH
#unset LD_LIBRARY_PATH


from transformers import AutoModelWithLMHead, AutoTokenizer
import os
directory = "models/checkpoint-44000"
# directory = "models/checkpoint-50000"
model = AutoModelWithLMHead.from_pretrained(directory)
tokenizer = AutoTokenizer.from_pretrained(directory)
out = "/home/juno/repos/huggingface.co/remotejob/tweetsT5_small_sum_fi"
# out = "/home/juno/repos/huggingface.co/tweetsTINYGPT2fi_v1"
os.makedirs(out, exist_ok=True)
model.save_pretrained(out)
tokenizer.save_pretrained(out)


#python /home/juno/repos/github.com/guillaume-be/rust-bert/utils/convert_model.py /home/juno/repos/huggingface.co/remotejob/tweetsT5_small_sum_fi/pytorch_model.bin
#python /home/juno/repos/github.com/guillaume-be/rust-bert/utils/convert_model.py /home/juno/repos/huggingface.co/tweetsTINYGPT2fi_v1/pytorch_model.bin