from transformers import pipeline
from datasets import load_dataset


prod_data = load_dataset("csv", data_files=[
                                '../data/testcased.txt'], delimiter="\t", column_names=["ask"])
#check max_lengh !!!

summarizer = pipeline("summarization", model="models/checkpoint-72000", tokenizer="models/checkpoint-72000")

f = open("0seq2seqres.txt", "w")
for ask in prod_data['train']['ask']:
    # print(ask)


    sum=summarizer(ask, min_length=6, max_length=64)
    print(sum)
    f.write(ask + ' --> '+sum[0]['summary_text']+'\n')
