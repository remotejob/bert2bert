

#RUN2
export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp kernelcheckpoint/kernel-metadata.json.run2.remotejob kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip 
cp kernelcheckpoint/kernel-metadata.json.run2.sipvip kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/

#mybert ??
export KAGGLE_CONFIG_DIR=/home/juno/kagglesupportsupport
cp kernelgpu/kernel-metadata.json.run2.supportsupport kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/

export KAGGLE_CONFIG_DIR=/home/juno/kaggledevelopmentsupport
cp kernelcheckpoint/kernel-metadata.json.run2.developmentsupport kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp kernelcheckpoint/kernel-metadata.json.run2.almazurov kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/

unset KAGGLE_CONFIG_DIR
cp kernelcheckpoint/kernel-metadata.json.run2.alesandermazurov kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/