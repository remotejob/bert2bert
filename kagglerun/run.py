# -*- coding: utf-8 -*-
"""BERT2BERT for CNN/Dailymail

Original file is located at
    https://colab.research.google.com/drive/1Ekd5pUeCX7VOrMx94_czTkwNtLN32Uyu

***Note***: This notebook only uses a few training, validation, and test data samples for demonstration purposes.

### **Data Preprocessing**
"""
# https://anubhav20057.medium.com/step-by-step-guide-abstractive-text-summarization-using-roberta-e93978234a90
# from shutil import copyfile

import os
import sys
# os.system('pip install --upgrade transformers==4.0.1')
os.system('pip install --upgrade transformers')
os.system('pip install --upgrade datasets')
os.system('pip install rouge_score')

sys.path.append("../input/seq2seqtrn")

import torch

from transformers import BertTokenizer, EncoderDecoderModel
from typing import Optional
from dataclasses import dataclass, field
from transformers import TrainingArguments
from seq2seq_trainer import Seq2SeqTrainer
import wandb
from transformers import BertTokenizerFast
import transformers
import datasets
from datasets import Dataset
from datasets import load_dataset
import pandas as pd


trainfile ='../input/seq2seqtrn/split/x07'
# trainfile ='../input/seq2seqtrn/test250000.tsv'
save_steps = 500000
eval_steps = 500
warmup_steps= 500
best = 0.0

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
wandb.init(project="bert2bert",config={"epochs": 1.0, "trainfile":trainfile,"save_steps":save_steps,"eval_steps":eval_steps,"warmup_steps":warmup_steps,"best":best})

tokenizer = BertTokenizerFast.from_pretrained(
    "TurkuNLP/bert-base-finnish-cased-v1")
tokenizer.bos_token = tokenizer.cls_token
tokenizer.eos_token = tokenizer.sep_token

# train_data = pd.read_csv(wandb.config.trainfile, sep='\t')
# train_data.columns=["ask", "answer"]
# print("train-size",train_data.size)
# val_data =train_data[train_data.index % 100 == 0] 
# val_data.columns=["ask", "answer"]
# print("val-size",val_data.size)
# val_data = Dataset.from_pandas(val_data)
# train_data = Dataset.from_pandas(train_data)

train_data =load_dataset("csv", data_files=[wandb.config.trainfile], delimiter="\t", column_names=["ask", "answer"], script_version="master")
val_data =load_dataset("csv", data_files=[wandb.config.trainfile+"_val"], delimiter="\t", column_names=["ask", "answer"], script_version="master")

batch_size = 48 #10
# encoder_max_length = 512
# decoder_max_length = 128

encoder_max_length = 21 #maybe 23 ?
decoder_max_length = 21


def process_data_to_model_inputs(batch):
    # tokenize the inputs and labels
    inputs = tokenizer(batch["ask"], padding="max_length",
                       truncation=True, max_length=encoder_max_length)
    outputs = tokenizer(batch["answer"], padding="max_length",
                        truncation=True, max_length=decoder_max_length)

    batch["input_ids"] = inputs.input_ids
    batch["attention_mask"] = inputs.attention_mask
    batch["decoder_input_ids"] = outputs.input_ids
    batch["decoder_attention_mask"] = outputs.attention_mask
    batch["labels"] = outputs.input_ids.copy()

    # because BERT automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # We have to make sure that the PAD token is ignored
    batch["labels"] = [[-100 if token == tokenizer.pad_token_id else token for token in labels]
                       for labels in batch["labels"]]

    return batch

val_data = val_data.map(
    process_data_to_model_inputs,
    batched=True,
    batch_size=batch_size,
    remove_columns=["ask", "answer"]
)
val_data.set_format(
    type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
)
# val_data = torch.utils.data.DataLoader(val_data, batch_size=36)

train_data = train_data.map(
    process_data_to_model_inputs,
    batched=True,
    batch_size=batch_size,
    remove_columns=["ask", "answer"]
)
train_data.set_format(
    type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
)
# train_data = torch.utils.data.DataLoader(train_data, batch_size=36)

bert2bert = EncoderDecoderModel.from_encoder_decoder_pretrained(
    "TurkuNLP/bert-base-finnish-cased-v1", "TurkuNLP/bert-base-finnish-cased-v1")

# set special tokens
bert2bert.config.decoder_start_token_id = tokenizer.bos_token_id
bert2bert.config.eos_token_id = tokenizer.eos_token_id
bert2bert.config.pad_token_id = tokenizer.pad_token_id

# sensible parameters for beam search
bert2bert.config.vocab_size = bert2bert.config.decoder.vocab_size
bert2bert.config.max_length = 142
bert2bert.config.min_length = 56
bert2bert.config.no_repeat_ngram_size = 3
bert2bert.config.early_stopping = True
bert2bert.config.length_penalty = 2.0
bert2bert.config.num_beams = 4


@dataclass
class Seq2SeqTrainingArguments(TrainingArguments):
    # def Seq2SeqTrainingArguments(TrainingArguments):
    label_smoothing: Optional[float] = field(
        default=0.0, metadata={"help": "The label smoothing epsilon to apply (if not zero)."}
    )
    sortish_sampler: bool = field(default=False, metadata={
                                  "help": "Whether to SortishSamler or not."})
    predict_with_generate: bool = field(
        default=False, metadata={"help": "Whether to use generate to calculate generative metrics (ROUGE, BLEU)."}
    )
    adafactor: bool = field(default=False, metadata={
                            "help": "whether to use adafactor"})
    encoder_layerdrop: Optional[float] = field(
        default=None, metadata={"help": "Encoder layer dropout probability. Goes into model.config."}
    )
    decoder_layerdrop: Optional[float] = field(
        default=None, metadata={"help": "Decoder layer dropout probability. Goes into model.config."}
    )
    dropout: Optional[float] = field(default=None, metadata={
                                     "help": "Dropout probability. Goes into model.config."})
    attention_dropout: Optional[float] = field(
        default=None, metadata={"help": "Attention dropout probability. Goes into model.config."}
    )
    lr_scheduler: Optional[str] = field(
        default="linear", metadata={"help": f"Which lr scheduler to use."}
    )


# load rouge for validation
rouge = datasets.load_metric("rouge")


def compute_metrics(pred):
    labels_ids = pred.label_ids
    pred_ids = pred.predictions

    # all unnecessary tokens are removed
    pred_str = tokenizer.batch_decode(pred_ids, skip_special_tokens=True)
    labels_ids[labels_ids == -100] = tokenizer.pad_token_id
    label_str = tokenizer.batch_decode(labels_ids, skip_special_tokens=True)

    rouge_output = rouge.compute(
        predictions=pred_str, references=label_str, rouge_types=["rouge2"])["rouge2"].mid

    best = round(rouge_output.precision, 4)


    if best > wandb.config.best:
        print("SAVE BEST",best,wandb.config.best)
        bert2bert.save_pretrained("./best_model")  
        wandb.config.update({"epochs": 1.0, "trainfile":trainfile,"save_steps":save_steps,"eval_steps":eval_steps,"warmup_steps":warmup_steps,"best":best},allow_val_change=True)
    else:
        print("DONT SAVE BEST",best,wandb.config.best)

    wandb.log({'rouge2_precision': best}) 

    return {
        "rouge2_precision": round(rouge_output.precision, 4),
        "rouge2_recall": round(rouge_output.recall, 4),
        "rouge2_fmeasure": round(rouge_output.fmeasure, 4),
    }


# set training arguments - these params are not really tuned, feel free to change
training_args = Seq2SeqTrainingArguments(
    predict_with_generate=True, 
    evaluation_strategy="steps", #new
    output_dir="./model",
    per_device_train_batch_size=batch_size,
    per_device_eval_batch_size=batch_size,
    # predict_with_generate=True,
    # evaluate_during_training=True,
    do_train=True,
    do_eval=True,
    num_train_epochs=wandb.config.epochs,
    # max_steps=numrecords,
    # max_steps=1500,
    # logging_steps=2,  # set to 1000 for full training
    # save_steps=16,  # set to 500 for full training
    # eval_steps=4,  # set to 8000 for full training
    # warmup_steps=1,  # set to 2000 for full training
    # max_steps=16, # delete for full training
    # logging_steps=1000,  # set to 1000 for full training
    # save_steps=1000,  # set to 500 for full training
    # eval_steps=8000,  # set to 8000 for full training
    # warmup_steps=2000,  # set to 2000 for full training
    # logging_steps=1000,  # set to 1000 for full training
    logging_steps=0,
    save_steps=wandb.config.save_steps,  # set to 500 for full training
    eval_steps=wandb.config.eval_steps,  # set to 8000 for full training
    warmup_steps=wandb.config.warmup_steps,  # set to 2000 for full training
    overwrite_output_dir=True,
    save_total_limit=1,
    fp16=True,
    run_name='bert2bert',

)

# instantiate trainer
trainer = Seq2SeqTrainer(
    model=bert2bert,
    args=training_args,
    compute_metrics=compute_metrics,
    train_dataset=train_data['train'],
    eval_dataset=val_data['train'],
)
trainer.train()

"""### **Evaluation**

Awesome, we finished training our dummy model. Let's now evaluated the model on the test data. We make use of the dataset's handy `.map()` function to generate a summary of each sample of the test data.
"""

if os.path.isdir("./best_model"):

    test_data = pd.read_csv("../input/seq2seqtrn/askans_test.tsv", sep='\t')
    test_data.columns=["ask", "answer"]
    print("test-size",test_data.size)
    test_data = Dataset.from_pandas(test_data)

    tokenizer = BertTokenizer.from_pretrained(
        "TurkuNLP/bert-base-finnish-cased-v1")
    model = EncoderDecoderModel.from_pretrained("./best_model")
    model.to("cuda")

    test_data = test_data.select(range(10))

    inputs = tokenizer(test_data["ask"], padding="max_length",
                    truncation=True, max_length=21, return_tensors="pt")
    input_ids = inputs.input_ids.to("cuda")
    attention_mask = inputs.attention_mask.to("cuda")
    outputs = model.generate(input_ids, attention_mask=attention_mask)
    output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)

    for x in range(10):
        print(test_data[x]["ask"])
        print('--->')
        print(test_data[x]["answer"])
        print('--->>>RES')    
        print(output_str[x])
        print('------------------------')




# batch_size = 16  # change to 64 for full evaluation

# # map data correctly
# def generate_summary(batch):
#     # Tokenizer will automatically set [BOS] <text> [EOS]
#     # cut off at BERT max length 512
#     inputs = tokenizer(batch["ask"], padding="max_length", truncation=True, max_length=512, return_tensors="pt")
#     input_ids = inputs.input_ids.to("cuda")
#     attention_mask = inputs.attention_mask.to("cuda")

#     outputs = model.generate(input_ids, attention_mask=attention_mask)

#     # all special tokens including will be removed
#     output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)
#     print(output_str)

#     batch["pred"] = output_str

#     return batch

# results = test_data.map(generate_summary, batched=True, batch_size=batch_size, remove_columns=["ask"])

# pred_str = results["pred"]
# label_str = results["answer"]

# rouge_output = rouge.compute(predictions=pred_str, references=label_str, rouge_types=["rouge2"])["rouge2"].mid

# print(rouge_output)

# GOOD SCORE 18.2 ruge
