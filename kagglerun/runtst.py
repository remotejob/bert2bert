# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python Docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import urllib.request as request
from urllib.parse import urlencode

import os
import sys
import threading
from tqdm import tqdm 
# import wandb

os.system('pip install transformers -U')
os.system('pip install --upgrade datasets')

import torch
from transformers import BertTokenizer,EncoderDecoderModel,BertTokenizerFast
from datasets import load_dataset
# import wandb


class Logger(object):
    CHANNEL_NAME = 'sipvip'

    def __init__(self):
        self.terminal = sys.stdout

    def write(self, message):
        if message != '\n':
            self.terminal.write(message + '\n')
            payload = {'msg': message}
            quoted = urlencode(payload)
            thr = threading.Thread(target=self.send, args=(quoted,), kwargs={})
            thr.start()

    def flush(self):
        pass

    @staticmethod
    def send(msg):
        msg = 'https://dweet.io/dweet/for/' + Logger.CHANNEL_NAME + '?' + msg
        try:
            request.urlopen(msg).read()
        except Exception as e:
            sys.stdout.terminal.write(e)

sys.stdout = Logger()

tokenizer = BertTokenizerFast.from_pretrained(
    "TurkuNLP/bert-base-finnish-cased-v1")
tokenizer.bos_token = tokenizer.cls_token
tokenizer.eos_token = tokenizer.sep_token

prod_data =load_dataset("csv", data_files=['../input/seq2seqtrn/testcased.txt'],delimiter="\t",column_names=["ask"], script_version="master")

model = EncoderDecoderModel.from_pretrained("../input/seq2seqtrn/best_model")
model.to("cuda")

f = open("0seq2seqres.txt", "w")
        
for ask in prod_data['train']['ask']:
    inputs = tokenizer(ask, padding="max_length",truncation=True, max_length=21, return_tensors="pt")
    input_ids = inputs.input_ids.to("cuda")
    attention_mask = inputs.attention_mask.to("cuda")
    outputs = model.generate(input_ids, attention_mask=attention_mask)
    output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)
    f.write(ask + ' --> '+output_str[0]+'\n')

    input_ids = torch.tensor(tokenizer.encode(ask, add_special_tokens=True)).unsqueeze(0).to("cuda")
    generated = model.generate(input_ids, decoder_start_token_id=model.config.decoder.pad_token_id)
    output_str = tokenizer.batch_decode(generated, skip_special_tokens=True)
    f.write('second --------'+'\n')
    
    for phrase in output_str:
        f.write(phrase+'\n')
    f.write('end --------'+'\n\n')
# for ask in prod_data['train']['ask']:
#     # inputs = tokenizer(ask, padding="max_length",truncation=True, max_length=21, return_tensors="pt")
#     input_ids = torch.tensor(tokenizer.encode(ask, add_special_tokens=True)).unsqueeze(0).to("cuda")
#     # input_ids = input_ids.to("cuda")
#     # attention_mask = inputs.attention_mask.to("cuda")
#     outputs = model.generate(input_ids, attention_mask=attention_mask)
#     output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)
#     print(ask +" ------------->\n")
#     for phrase in output_str:
#         f.write(phrase+'\n')
#     print("-----------------------\n")





# batch_size = 48
encoder_max_length = 21
decoder_max_length = 21



def process_data_to_model_inputs(batch):
    # print("batch ask",batch["ask"])
    # print("batch anwer",batch["answer"])
    inputs = tokenizer(batch["ask"], padding="max_length",truncation=True, max_length=encoder_max_length)
    outputs = tokenizer(batch["answer"], padding="max_length",truncation=True, max_length=decoder_max_length)

    batch["input_ids"] = inputs.input_ids
    batch["attention_mask"] = inputs.attention_mask
    batch["decoder_input_ids"] = outputs.input_ids
    batch["decoder_attention_mask"] = outputs.attention_mask
    batch["labels"] = outputs.input_ids.copy()

    # because BERT automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # We have to make sure that the PAD token is ignored
    batch["labels"] = [[-100 if token == tokenizer.pad_token_id else token for token in labels]
                       for labels in batch["labels"]]

    return batch



# import torch
# from transformers import AutoTokenizer

# # os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
# epochs =1 
# trainfile ='../input/seq2seqtrn/split/x00'
# # trainfile ='../input/seq2seqtrn/askans_train.tsv'
# save_steps = 5000000
# eval_steps = 500
# warmup_steps= 500
# best=0.00302

# # wandb.init(project="bert2berttst",config={"epochs": 1.0, "trainfile":trainfile,"save_steps":save_steps,"eval_steps":eval_steps,"warmup_steps":warmup_steps,"best":best})

# dataset =load_dataset("csv", data_files=[trainfile], delimiter="\t", column_names=["ask", "answer"], script_version="master")

# dataset = dataset['train'].select(range(2000))
# dataset = dataset['train']

# tokenizer = AutoTokenizer.from_pretrained('TurkuNLP/bert-base-finnish-cased-v1')
# tokenizer.bos_token = tokenizer.cls_token
# tokenizer.eos_token = tokenizer.sep_token
# # tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
# dataset = dataset.map(process_data_to_model_inputs, batched=True,remove_columns=["ask", "answer"])

# dataset.set_format(
#     type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
# )

# input_ids = torch.tensor(tokenizer.encode("Voi apua", add_special_tokens=True)).unsqueeze(0)

# model = EncoderDecoderModel.from_encoder_decoder_pretrained('TurkuNLP/bert-base-finnish-cased-v1', 'TurkuNLP/bert-base-finnish-cased-v1')
# # model = EncoderDecoderModel.from_encoder_decoder_pretrained('bert-base-uncased', 'bert-base-uncased') 
# model.to("cuda")

# # dataset.set_format(type='torch', columns=['input_ids', 'token_type_ids', 'attention_mask', 'label'])
# dataloader = torch.utils.data.DataLoader(dataset, batch_size=275) #275 max ?
# # for epoch in range(epochs):
# #     bathid = 0
# for batch in dataloader:
#     # bathid += 1
#     # wandb.log({'bathid': bathid})
#     input_ids = batch['input_ids'].to("cuda")
#     decoder_input_ids =batch['decoder_input_ids'].to("cuda")
#     attention_mask = batch['attention_mask'].to("cuda")
#     decoder_attention_mask =batch['decoder_input_ids'].to("cuda")
#     labels=batch['labels'].to("cuda")
#     model(input_ids=input_ids, decoder_input_ids=decoder_input_ids, attention_mask=attention_mask,decoder_attention_mask =decoder_attention_mask,labels=labels)


# model.save_pretrained("bert2bert")
# model = EncoderDecoderModel.from_pretrained("bert2bert")
# model.to("cuda")

# generated = model.generate(input_ids, decoder_start_token_id=model.config.decoder.pad_token_id)

# output_str = tokenizer.batch_decode(generated, skip_special_tokens=True)

# for phrase in output_str:
#     print(phrase+'\n')

# print('Second --------------------')
# phrasein = "Mitä pyhempi maa sen parempi."
# inputs = tokenizer(phrasein, padding="max_length", truncation=True, max_length=21, return_tensors="pt")
# input_ids = inputs.input_ids.to("cuda")
# attention_mask = inputs.attention_mask.to("cuda")
# outputs = model.generate(input_ids, attention_mask=attention_mask,decoder_start_token_id=model.config.decoder.pad_token_id)
# output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)

# print("IN-->",phrasein +'\n')
# for phrase in output_str:
#     print(phrase+'\n')




