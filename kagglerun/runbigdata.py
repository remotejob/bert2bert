#https://huggingface.co/docs/datasets/processing.html map dataset!!

import os
import sys

# os.system('pip install --upgrade transformers==4.1.1')
# os.system('pip install --upgrade datasets==1.1.3')
os.system('pip install --upgrade git+https://github.com/intake/filesystem_spec')
os.system('pip install --upgrade datasets')


os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')


#tstpplars.tsv size 1650001 3299970
#askans_train.tsv 10666797 21333420

from transformers import BertTokenizerFast
from transformers import EncoderDecoderModel
# from dataclasses import dataclass, field
# from transformers import TrainingArguments
from datasets import load_dataset
from datasets import Dataset
import pandas as pd
from pathlib import Path
# import datasets
import wandb
import gc

start =False
runnum = "4"
acc = "almazurov"
# trainfile = '../input/seq2seqtrn/tstpplars.tsv'
# trainfile = '../input/seq2seqtrn/askans_train.tsv'
trainfile = '../input/seq2seqtrn/spliteddata/train00.tsv'
# trainfile = 'data/train19.tsv'
epochs = 1# 1.40 Fin 0.5 for empry runnum 1 last 0.9 0.77 0.3/5.38 2.30
save_steps = 300
eval_steps = 300
warmup_steps = 200
best = 0.00
batch_size = 22  # 256/275/224 for finns 100 Roberta 128 multiling 186
# pretrainedmodel = 'roberta-base'
# pretrainedmodel = 'xlm-roberta-base'
pretrainedmodel = 'TurkuNLP/bert-base-finnish-cased-v1'
# pretrainedmodel = 'Helsinki-NLP/opus-mt-fi-fi'
# pretrainedmodel = 'bert-base-multilingual-cased'

run = wandb.init(project="bigdata", entity='remotejob2', config={"epochs": epochs, "trainfile": trainfile, "save_steps": save_steps,
                                                                    "eval_steps": eval_steps, "warmup_steps": warmup_steps, "best": best, 'batch_size': batch_size, 'runnum': runnum, 'acc': acc})

# tr_path = Path('../input/seq2seqtrn/spliteddata').glob('train0*.tsv')
tr_path = Path('../input/seq2seqtrn/spliteddata')

# path = Path('../input/seq2seqtrn/spliteddata'

dforg = pd.read_csv(tr_path, sep="\t",names=["ask", "answer"])
# dforg = pd.read_csv('../input/seq2seqtrn/spliteddata...tsv')

df = dforg.dropna()
# df = dforg.sample(frac=1).reset_index(drop=True)

# df = dforg[:4000]

# df.to_csv('pd_train.csv',index = False)


train_data = Dataset.from_pandas(df)

print(train_data)

print("SIZE",df.size)
print(df.info(memory_usage='deep'))


# train_data = Dataset.from_pandas(df)
# val_data = Dataset.from_pandas(df[:4000])

del df
gc.collect()

tokenizer = BertTokenizerFast.from_pretrained(pretrainedmodel,use_auth=False)
tokenizer.bos_token = tokenizer.cls_token
tokenizer.eos_token = tokenizer.sep_token

batch_size = wandb.config.batch_size

encoder_max_length = 132
decoder_max_length = 125



small_dataset = train_data.select(range(10))

print(small_dataset)

small_dataset.map(lambda example: print(len(example['answer'])))

small_dataset.map(lambda example: print(example['answer']))



def add_prefix(example):

    example['answer'] = 'My sentence: ' + example['answer']
    return example


updated_dataset = small_dataset.map(add_prefix)

updated_dataset.map(lambda example: print(example['answer']))


print(updated_dataset.column_names)

updated_dataset = small_dataset.map(lambda example: {'new_sentence': example['answer']}, remove_columns=['answer'])

print(updated_dataset.column_names)




def process_data_to_model_inputs(batch):
    # tokenize the inputs and labels

    inputs = tokenizer(batch["ask"], padding="max_length",
                       truncation=True, max_length=encoder_max_length)
    outputs = tokenizer(batch["answer"], padding="max_length",
                        truncation=True, max_length=decoder_max_length)

    batch["input_ids"] = inputs.input_ids
    batch["attention_mask"] = inputs.attention_mask
    batch["decoder_input_ids"] = outputs.input_ids
    batch["decoder_attention_mask"] = outputs.attention_mask
    batch["labels"] = outputs.input_ids.copy()

    # because RoBERTa automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # We have to make sure that the PAD token is ignored
    batch["labels"] = [[-100 if token == tokenizer.pad_token_id else token for token in labels]
                       for labels in batch["labels"]]

    return batch

# tsvtrain_data = load_dataset("csv", data_files=[
#                                 './pd_train.csv'],column_names=["ask","answer"])


# os.remove("./pd_train.csv")
# tsvtrain_data = load_dataset("csv", data_files=[
#                                 '../input/seq2seqtrn/train19.tsv'], delimiter="\t", column_names=["ask","answer"])


# train_data = load_dataset("csv", data_files=[
#                                 '../input/seq2seqtrn/train19.tsv'], delimiter="\t")


# print(tsvtrain_data['train'])

# train_data = Dataset.from_pandas(df)
# del df
# gc.collect()
# train_data = tsvtrain_data['train'].map(
#     process_data_to_model_inputs,
#     batched=True,
#     batch_size=batch_size,
#     remove_columns=["ask", "answer"]
# )
# train_data.set_format(
#     type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
# )

# train_data.save_to_disk('./train_data')

# val_data = Dataset.from_pandas(df[:4000])
# val_data = val_data.map(
#     process_data_to_model_inputs,
#     batched=True,
#     batch_size=batch_size,
#     remove_columns=["ask", "answer"]
# )
# val_data.set_format(
#     type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
# )





# model_shared = EncoderDecoderModel.from_encoder_decoder_pretrained(
#         pretrainedmodel, pretrainedmodel, tie_encoder_decoder=True) 


# model_shared.config.decoder_start_token_id = tokenizer.bos_token_id
# model_shared.config.eos_token_id = tokenizer.eos_token_id

# # sensible parameters for beam search
# # set decoding params
# model_shared.config.max_length = 40
# model_shared.config.early_stopping = True
# model_shared.config.no_repeat_ngram_size = 3
# model_shared.config.length_penalty = 2.0
# model_shared.config.num_beams = 4
# model_shared.config.vocab_size = model_shared.config.encoder.vocab_size        