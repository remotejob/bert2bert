# ApPW0jmKXzW83jDVDPpDdkLh4

import os
import sys
import shutil

os.system('pip install --upgrade transformers==4.1.1')
os.system('pip install --upgrade datasets==1.1.3')
# os.system('pip install --upgrade tensorflow-gpu')
os.system('pip install --upgrade tensorflow==2.3.2')
os.system('pip install -U rouge_score')
os.system('pip install -U wandb')
# os.system('pip install -U py-polars')
# os.system('pip install dask')  

sys.path.append("../input/seq2seqtrn")

# from comet_ml import Experiment
from transformers import BertTokenizer, BertTokenizerFast
import wandb
from typing import Optional
from dataclasses import dataclass, field
from transformers import TrainingArguments
from seq2seq_trainer import Seq2SeqTrainer
from datasets import load_dataset
from transformers import EncoderDecoderModel
from datasets import Dataset
import pandas as pd
import transformers
import datasets
import json
from pathlib import Path
import glob
import shutil
import time

os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

transformers.logging.set_verbosity_info()


class MyCallback(transformers.TrainerCallback):

    def on_train_begin(self, args, state, control, **kwargs):
        print("Starting training")
        self.startTime = time.time()

    def on_save(self, args, state, control, **kwargs):
 
        self.elapsedTime = time.time() - self.startTime  

        if self.elapsedTime > 30000:
            control.should_training_stop =  True


start =False
runnum = "39"
acc = "supportsupport"
trainfile = '../input/seq2seqtrn/spliteddata/train39.tsv'
epochs = 2# 1.40 Fin 0.5 for empry runnum 1 last 0.9 0.77 0.3/5.38 2.30
save_steps = 300
eval_steps = 300
warmup_steps = 200
best = 0.00
batch_size = 27  # 256/275/224 for finns 100 Roberta 128 multiling 186
# pretrainedmodel = 'roberta-base'
# pretrainedmodel = 'xlm-roberta-base'
pretrainedmodel = 'TurkuNLP/bert-base-finnish-cased-v1'
# pretrainedmodel = 'Helsinki-NLP/opus-mt-fi-fi'
# pretrainedmodel = 'bert-base-multilingual-cased'

run = wandb.init(project="bert2bertcheckpoint", entity='remotejob2', config={"epochs": epochs, "trainfile": trainfile, "save_steps": save_steps,
                                                                    "eval_steps": eval_steps, "warmup_steps": warmup_steps, "best": best, 'batch_size': batch_size, 'runnum': runnum, 'acc': acc})


if runnum != '':
    artifactdir = 'remotejob2/bert2bertcheckpoint/bert2bert_checkpointmodel:v'+runnum
    artifact = run.use_artifact(artifactdir, type='model')
    artifact_dir = artifact.download()

    fjson = open(artifact_dir+'/best.json', 'r')
    datajson = json.load(fjson)

    oldbest = datajson['best'][0]['precision']
    print("bestjson?", oldbest)
    shutil.move(artifact_dir,'./last_model')

    shutil.copyfile('../input/seq2seqtrn/vocab.txt','./last_model/vocab.txt')

    pretrainedmodel='./last_model'

    wandb.config.update({"epochs": epochs, "trainfile": trainfile, "save_steps": save_steps,
                            "eval_steps": eval_steps, "warmup_steps": warmup_steps, "best": oldbest}, allow_val_change=True)


print("START from:",pretrainedmodel)


dforg = pd.read_csv(wandb.config.trainfile, sep="\t", names=["ask", "answer"])
# dforg = pd.read_csv(paths, sep="\t", names=["ask", "answer"])
dforg = dforg.dropna()
df = dforg.sample(frac=1).reset_index(drop=True)

#pypolars
# df = pl.read_csv(wandb.config.trainfile, sep="\t", names=["ask", "answer"])
# df = pl.scan_csv(wandb.config.trainfile, sep="\t", columns=["ask", "answer"])
# df = pl.read_csv(wandb.config.trainfile, sep="\t",ignore_errors=True)
# dfp = df.to_pandas()
########################################################################
#DASK TEST
# df  = dd.read_csv(wandb.config.trainfile, sep="\t", names=["ask", "answer"])

train_data = Dataset.from_pandas(df)
val_data = Dataset.from_pandas(df[:4000])
# test_data=Dataset.from_pandas(df[456000:456010])


# tok#enizer = RobertaTokenizerFast.from_pretrained(pretrainedmodel)
tokenizer = BertTokenizerFast.from_pretrained(pretrainedmodel,use_auth=False)
tokenizer.bos_token = tokenizer.cls_token
tokenizer.eos_token = tokenizer.sep_token
# parameter setting
batch_size = wandb.config.batch_size
# max_length !!!! related to data size (ask,answer)
# encoder_max_length = 30 ask
# decoder_max_length = 20 ans
# big change 12.05.2021
# cat ~/gowork/src/gitlab.com/supportsupport/mltwitterv3/data/longest.txt
encoder_max_length = 64
decoder_max_length = 32

def process_data_to_model_inputs(batch):
    # tokenize the inputs and labels

    inputs = tokenizer(batch["ask"], padding="max_length",
                       truncation=True, max_length=encoder_max_length)
    outputs = tokenizer(batch["answer"], padding="max_length",
                        truncation=True, max_length=decoder_max_length)

    batch["input_ids"] = inputs.input_ids
    batch["attention_mask"] = inputs.attention_mask
    batch["decoder_input_ids"] = outputs.input_ids
    batch["decoder_attention_mask"] = outputs.attention_mask
    batch["labels"] = outputs.input_ids.copy()

    # because RoBERTa automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # We have to make sure that the PAD token is ignored
    batch["labels"] = [[-100 if token == tokenizer.pad_token_id else token for token in labels]
                       for labels in batch["labels"]]
    return batch


train_data = train_data.map(
    process_data_to_model_inputs,
    batched=True,
    batch_size=batch_size,
    remove_columns=["ask", "answer"]
)
train_data.set_format(
    type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
)

val_data = val_data.map(
    process_data_to_model_inputs,
    batched=True,
    batch_size=batch_size,
    remove_columns=["ask", "answer"]
)
val_data.set_format(
    type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
)

if start and runnum == '':
    print('BEGINING!!! of all')
    model_shared = EncoderDecoderModel.from_encoder_decoder_pretrained(
        pretrainedmodel, pretrainedmodel, tie_encoder_decoder=True)  # for first time!!

else:
    # print("EncoderDecoderModel.from_pretrained",artifact_dir,pretrainedmodel)
    model_shared = EncoderDecoderModel.from_pretrained(pretrainedmodel)

model_shared.config.decoder_start_token_id = tokenizer.bos_token_id
model_shared.config.eos_token_id = tokenizer.eos_token_id

# sensible parameters for beam search
# set decoding params
model_shared.config.max_length = 40
model_shared.config.early_stopping = True
model_shared.config.no_repeat_ngram_size = 3
model_shared.config.length_penalty = 2.0
model_shared.config.num_beams = 4
model_shared.config.vocab_size = model_shared.config.encoder.vocab_size


@dataclass
class Seq2SeqTrainingArguments(TrainingArguments):
    # def Seq2SeqTrainingArguments(TrainingArguments):
    label_smoothing: Optional[float] = field(
        default=0.0, metadata={"help": "The label smoothing epsilon to apply (if not zero)."}
    )
    sortish_sampler: bool = field(default=False, metadata={
                                  "help": "Whether to SortishSamler or not."})
    predict_with_generate: bool = field(
        default=False, metadata={"help": "Whether to use generate to calculate generative metrics (ROUGE, BLEU)."}
    )
    adafactor: bool = field(default=False, metadata={
                            "help": "whether to use adafactor"})
    encoder_layerdrop: Optional[float] = field(
        default=None, metadata={"help": "Encoder layer dropout probability. Goes into model.config."}
    )
    decoder_layerdrop: Optional[float] = field(
        default=None, metadata={"help": "Decoder layer dropout probability. Goes into model.config."}
    )
    dropout: Optional[float] = field(default=None, metadata={
                                     "help": "Dropout probability. Goes into model.config."})
    attention_dropout: Optional[float] = field(
        default=None, metadata={"help": "Attention dropout probability. Goes into model.config."}
    )
    lr_scheduler: Optional[str] = field(
        default="linear", metadata={"help": f"Which lr scheduler to use."}
    )


rouge = datasets.load_metric("rouge")


def compute_metrics(pred):
 
    labels_ids = pred.label_ids
    pred_ids = pred.predictions

    # all unnecessary tokens are removed
    pred_str = tokenizer.batch_decode(pred_ids, skip_special_tokens=True)
    labels_ids[labels_ids == -100] = tokenizer.pad_token_id
    label_str = tokenizer.batch_decode(labels_ids, skip_special_tokens=True)

    rouge_output = rouge.compute(
        predictions=pred_str, references=label_str, rouge_types=["rouge2"])["rouge2"].mid

    best = round(rouge_output.precision, 7)

    precision = round(rouge_output.precision, 7)
    recall = round(rouge_output.recall, 7),
    fmeasure = round(rouge_output.fmeasure, 7)

    if best > wandb.config.best:
        print("SAVE BEST", best, wandb.config.best)
        model_shared.save_pretrained("./best_model")
        # experiment.log_model("BEST_MODEL2", "./best_model",overwrite=True)
        bestjson = {}
        bestjson['best'] = []
        bestjson['best'].append(
            {'precision': best, 'recall': recall[0], 'fmeasure': fmeasure})
        with open('./best_model/best.json', 'w') as outfile:
            json.dump(bestjson, outfile)
 
        wandb.config.update({"epochs": epochs, "trainfile": trainfile, "save_steps": save_steps,
                             "eval_steps": eval_steps, "warmup_steps": warmup_steps, "best": best, 'runnum': runnum, 'acc': acc}, allow_val_change=True)
    else:
        print("DONT SAVE BEST", best, wandb.config.best)

    wandb.log({'precision': precision,
               'recall': recall[0], 'fmeasure': fmeasure})

    # experiment.log_metric('precision',precision)
    return {
        "rouge2_precision": precision,
        "rouge2_recall": recall[0],
        "rouge2_fmeasure": fmeasure,
    }


training_args = Seq2SeqTrainingArguments(
    evaluation_strategy="steps",
    output_dir="./",
    per_device_train_batch_size=batch_size,
    per_device_eval_batch_size=batch_size,
    predict_with_generate=True,
    # evaluate_during_training=True,
    num_train_epochs=wandb.config.epochs,
    do_train=True,
    do_eval=True,
    logging_steps=10,
    save_steps=wandb.config.save_steps,
    eval_steps=wandb.config.eval_steps,
    warmup_steps=wandb.config.warmup_steps,
    overwrite_output_dir=True,
    save_total_limit=1,
    fp16=True,
)

# instantiate trainer
trainer = Seq2SeqTrainer(
    model=model_shared,
    args=training_args,
    compute_metrics=compute_metrics,
    train_dataset=train_data,
    eval_dataset=val_data,
    callbacks=[MyCallback]
)

# print("num_train_epochs?",trainer.num_train_epochs)
# trainer.num_train_epochs = trainer.num_train_epochs + 5 ???


trainer.train()

modeldir = ''

for entry in os.scandir(path='.'):
    if entry.name.startswith('checkpoint'):
        print('checkpoint',entry.name)
        modeldir = entry.name


print('modeldir--->',modeldir)
if modeldir != '':
    # run = wandb.init(job_type='train')
    artifact = wandb.Artifact('bert2bert_checkpointmodel', type='model')

    for entry in os.scandir(path="./"+modeldir):
        print("wd", "./"+modeldir+"/"+entry.name)
        artifact.add_file("./"+modeldir+"/"+entry.name)
        # if os.path.isfile('./best_model/best.json'):
        #    artifact.add_file('./best_model/best.json')
    bestjson = {}
    bestjson['best'] = []
    bestjson['best'].append(
        {'precision': wandb.config.best})
    with open('./best.json', 'w') as outfile:
        json.dump(bestjson, outfile)

    artifact.add_file('./best.json')

    run.log_artifact(artifact)

    prod_data = load_dataset("csv", data_files=[
                                '../input/seq2seqtrn/testcased.txt'], delimiter="\t", column_names=["ask"])

    model = EncoderDecoderModel.from_pretrained("./"+modeldir)
    model.to("cuda")

    f = open("0seq2seqres.txt", "w")

    #check max_lengh !!!
    for ask in prod_data['train']['ask']:
        inputs = tokenizer(ask, padding="max_length",
                            truncation=True, max_length=30, return_tensors="pt")
        input_ids = inputs.input_ids.to("cuda")
        attention_mask = inputs.attention_mask.to("cuda")
        outputs = model.generate(input_ids, attention_mask=attention_mask)
        output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)
        f.write(ask + ' --> '+output_str[0]+'\n')


    f.close()


