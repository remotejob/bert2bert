#https://dzlab.github.io/dltips/en/tensorflow/create-bert-vocab/

from tokenizers import BertWordPieceTokenizer

# Initialize an empty BERT tokenizer
tokenizer = BertWordPieceTokenizer(
  clean_text=False,
  handle_chinese_chars=False,
  strip_accents=False,
  lowercase=True,
)

# prepare text files to train vocab on them
files = ['data/askans_train.tsv']

# train BERT tokenizer
tokenizer.train(
  files,
  vocab_size=30000,
  min_frequency=2,
  show_progress=True,
  special_tokens=['[PAD]', '[UNK]', '[CLS]', '[SEP]', '[MASK]'],
  limit_alphabet=1000,
  wordpieces_prefix="##"
)

# save the vocab
# tokenizer.save('.', 'bert')
tokenizer.model.save('dataorg/berttokenazer',None)