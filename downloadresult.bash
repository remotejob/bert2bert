rm -rf output/*

# export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
# kaggle kernels output remotejob/bert2bert -p output/

# export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
# kaggle kernels output sipvip/bert2bert -p output/

# unset KAGGLE_CONFIG_DIR
# kaggle kernels output alesandermazurov/bert2bert -p output/

# #RESUME
# export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
# kaggle kernels output remotejob/bert2bertresume -p output/

# export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
# kaggle kernels output sipvip/bert2bertresume -p output/

# export KAGGLE_CONFIG_DIR=/home/juno/kaggledevelopmentsupport
# kaggle kernels output developmentsupport/bert2bertresume -p output/



#RUN2 bert2bert2
rm -rf output/*

export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
kaggle kernels output remotejob/bert2bertcheckpoint -p output/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesupportsupport
kaggle kernels output supportsupport/bert2bertcheckpoint -p output/

export KAGGLE_CONFIG_DIR=/home/juno/kaggledevelopmentsupport
kaggle kernels output developmentsupport/bert2bertcheckpoint -p output/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
kaggle kernels output sipvip/bert2bertcheckpoint -p output/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
kaggle kernels output almazurov/bigdata -p output/

unset KAGGLE_CONFIG_DIR
kaggle kernels output alesandermazurov/bert2bertcheckpoint -p output/


rm data/best_model/*
# cp output/best_model/* data/best_model/
# cd output/
cp data/vocab.txt data/best_model/vocab.txt
cp best_model/* ../data/best_model/
cp output/checkpoint-10500/* data/best_model/
