#https://www.machinecurve.com/index.php/2021/02/16/easy-machine-translation-with-machine-learning-and-huggingface-transformers/
#https://www.gitmemory.com/issue/huggingface/transformers/8704/734644401
# from transformers import pipeline
from datasets import load_dataset
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM


prod_data = load_dataset("csv", data_files=[
                                '../data/testcased.txt'], delimiter="\t", column_names=["ask"])
#check max_lengh !!!


from transformers import AutoTokenizer, AutoModelForSeq2SeqLM

# Initialize the tokenizer
tokenizer = AutoTokenizer.from_pretrained("models/checkpoint-300000")


# Initialize the model
model = AutoModelForSeq2SeqLM.from_pretrained("models/checkpoint-300000")

f = open("0seq2seqres.txt", "w")
for ask in prod_data['train']['ask']:
    # print(ask)

    # tokenized_text = tokenizer.prepare_seq2seq_batch([ask], return_tensors='pt')
    # tokenized_text = tokenizer(ask, return_tensors="pt").input_ids
    # tokenized_text = tokenizer(ask, return_tensors="pt")
    tokenized_text = tokenizer.prepare_seq2seq_batch(src_texts=[ask], return_tensors="pt")

        # do_sample: true,
        # num_beams: 1,
        # top_k: 50,
        # top_p: 0.95,
        # no_repeat_ngram_size: 2,
        # early_stopping: true,

    # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=8, length_penalty=0.1)
    # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1)
    # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=8)  #OK 
    #translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=12,no_repeat_ngram_size=2,length_penalty=1.1,early_stopping=True)
    # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=1,no_repeat_ngram_size=2,length_penalty=1.1,top_k=50,top_p=0.95) #OK
    translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=3,no_repeat_ngram_size=2,length_penalty=1.1,top_k=50,top_p=0.95)

    # translation = model.generate(**tokenized_text)
    # translated_text = tokenizer.batch_decode(translation, skip_special_tokens=True)[0]

    translated_text = tokenizer.decode(translation[0], skip_special_tokens=True)

    print(ask + ' --> '+translated_text)
    f.write(ask + ' --> '+translated_text+'\n')
