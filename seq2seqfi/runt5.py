
# transformers 4.2!!
# python finetune_trainer.py --model sshleifer/distilbart-cnn-12-6 --output_dir models --data_dir cnn_dm --do_train --num_train_epochs 1 --overwrite_output_dir

# python finetune_trainer.py --model Helsinki-NLP/opus-mt-fi-fi --output_dir models --data_dir cnn_dm --do_train --num_train_epochs 1 --overwrite_output_dir


import os
import sys

os.system('pip install --upgrade git+https://github.com/intake/filesystem_spec')
os.system('pip install -U git+git://github.com/huggingface/transformers')
os.system('pip install --upgrade datasets')
os.system('pip install wandb')
os.system('pip install pandas')
os.system('git clone https://github.com/huggingface/transformers')
os.system('pip install nltk')
os.system('pip install rouge-score')
os.system('pip install sentencepiece')
os.system('pip install sacrebleu')
# os.system('wget https://raw.githubusercontent.com/huggingface/transformers/master/examples/seq2seq/seq2seq_trainer.py')
os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

run_clm_dir= './transformers/examples/pytorch/translation/'


cmd = '''python {0}run_translation.py \
  --model_name_or_path t5-small \
  --do_train --cache_dir /mnt/mldisk/cache --save_total_limit 1 --save_steps 1000 \
  --do_train \
  --do_eval \
  --train_file data/askans.json --validation_file data/askanseval.json \
  --per_device_train_batch_size=64 \
  --per_device_eval_batch_size=64  \
  --predict_with_generate --source_prefix "translate English to Finnish: " \
  --source_lang en --target_lang fi \
  --output_dir /mnt/mldisk/translation/models
'''.format(run_clm_dir)


print(cmd)

os.system(cmd)