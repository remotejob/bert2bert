import os
import pandas as pd
import json


for entry in os.scandir(path='.'):
    if entry.name.startswith('dataor'):
        print(entry.name)


df = pd.read_csv('data/askans_train.tsv', sep="\t", names=["ask", "answer"])
df = df.dropna()
print(df.head())

df_shuffled=df.sample(frac=1).reset_index(drop=True)
print(df_shuffled.head()) 


fjson = open('best.json','r')
datajson = json.load(fjson)

oldbest =datajson['best'][0]['precision']
print("bestjson?",oldbest)